<?php
class Controller {
	protected
		$database,
		$framework;

	public function __construct() {
		$this->framework = Base::instance();
		$this->database  = new DB\SQL($this->framework->get('DATABASE'));
	}

	public function days() {
		switch (date("m")) {
			case '08':
				$days = 31;
				break;
			default:
				$days = 30;
				break;
		}

		return $days;
	}
}

/* Akhir dari berkas controller.php */