<?php
class Handler extends DB\SQL\Mapper {
	public function __construct(DB\SQL $database, $table) {
		parent::__construct($database, $table);
	}

	public function loadAll() {
		$this->load();
		return $this->query;
	}

	public function createRecord() {
		$this->copyfrom('DATA');
		$this->save();
	}

	public function readRecord($col, $value, $container) {
		$this->load(["$col = ?", $value]);
		$this->copyto($container);
	}

	public function updateRecord($col, $value) {
		$this->load(["$col=?", $value]);
		$this->copyfrom('DATA');
		$this->update();
	}

	public function deleteRecord($id) {
		$this->load(['id=?', $id]);
		$this->erase();
	}

	public function countRecord($col, $value) {
		return $this->count(["$col = ?", $value]);
	}

	public function selectRecord($col, $value, $container) {
		$this->select("*", ["$col = ?", $value]);
		$this->copyto($container);
	}
}