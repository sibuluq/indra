<?php
class Router extends Controller {
	public function beforeRoute() {}

	public function afterRoute() {
		$menu_handler = new Handler($this->database, 'menu');
		$calculator   = new Calculator();
		//$math         = new Math();

		/*$this->framework->mset([
		'total'         => $math->calculateUsageMonthly('usage'),
		'average'       => $math->calculateAverageUsage('usage'),
		'forecast'      => $math->calculateUsageForecast(),
		'sisa'          => $math->calculateBalance(),
		'buffer'        => $math->calculateBufferStock(),
		'bufferday'     => $math->calculateBufferDays(),
		'buffertotal'   => $math->calculateBufferTotal(),
		'purchase_plan' => $math->calculatePurchasePlan(),
		'next_stock'    => $math->calculateNextStock(),
		'actual'        => $math->calculateNextStockApproved(),
		'current'       => $math->calculateUsageMonthly('monthlyusage'),
		'previous'      => $math->calculateUsagePreviousMonth()
		]);*/

		$this->framework->mset(array(
			'total'         => $calculator->calculateUsageMonthly('usage', 'month', date('Ym')),
			'average'       => $calculator->calculateUsageAverage('usage', 'month', date('Ym')),
			'forecast'      => $calculator->calculateUsageForecast('usage', 'month', date('Ym')),
			'sisa'          => $calculator->calculateBalance('usage', 'month', date('Ym')),
			'previous'      => $calculator->calculateBalancePreviousMonth(),
			'buffer'        => $calculator->calculateBuffer('usage', 'month', date('Ym')),
			'bufferday'     => $calculator->calculateBufferDays('usage', 'month', date('Ym')),
			'buffertotal'   => $calculator->calculateBufferTotal('usage', 'month', date('Ym')),
			'purchase_plan' => $calculator->calculatePurchasePlan('usage', 'month', date('Ym')),
			'next_stock'    => $calculator->calculateNextStock('usage', 'month', date('Ym')),
			'actual'        => $calculator->calculateNextStockApproved('usage', 'month', date('Ym')),
			'previous'      => $calculator->calculateUsagePreviousMonth()
		));

		$this->framework->set('menu', $menu_handler->loadAll());

		echo Template::instance()->render('base.html');
	}

	public function home() {
		$this->framework->set('page', 'home.html');
	}

	public function dailyUsage() {
		$usage_handler = new Handler($this->database, 'usage');

		$this->framework->mset([
			'usage'         => $usage_handler->loadAll(),
			'document'      => ['title' => 'Daily Usage'],
			'page'          => 'dailyusage.html'
		]);
	}

	public function monthlyUsage() {
		$usage_handler = new Handler($this->database, 'monthlyusage');

		$this->framework->mset([
			'usage'    => $usage_handler->loadAll(),
			'document' => ['title' => 'Monthly Usage'],
			'page'     => 'monthlyusage.html'
		]);
	}

	public function dailyProcessing() {
		$usage_handler = new Handler($this->database, 'usage');

		if ($this->framework->get('VERB') == 'POST') {
			$this->framework->set('DATA', array(
				'id'    => date('Ymd'),
				'year'  => date('Y'),
				'month' => date('Ym'),
				'date'  => date('d'),
				'usage' => $this->framework->get('POST.dailyusage'))
			);

			if ($usage_handler->countRecord('id', $this->framework->get('DATA.id')) > 0) {
				$usage_handler->updateRecord('id', $this->framework->get('DATA.id'));
			}
			else {
				$usage_handler->createRecord();
			}

			$this->framework->reroute($this->framework->get('POST.referrer'), false);
		}
		else {
			$this->framework->reroute('/', false);
		}
	}

	public function monthlyProcessing() {
		$usage_handler = new Handler($this->database, 'monthlyusage');

		if ($this->framework->get('VERB') == 'POST') {
			$this->framework->set('DATA', array(
				'month' => date("Ym"),
				'year'  => date("Y"),
				'label' => date("Y-m"),
				'usage' => $this->framework->get('POST.monthlyusage'))
			);

			if ($usage_handler->countRecord('month', $this->framework->get('DATA.month'))) {
				$usage_handler->updateRecord('month', $this->framework->get('DATA.month'));
			}
			else {
				$usage_handler->createRecord();
			}

			$this->framework->reroute($this->framework->get('POST.referrer'), false);
		}
		else {
			$this->framework->reroute('/', false);
		}
	}

	public function plan() {
		$handler = new Handler($this->database, 'purchasing');

		$handler->readRecord('id', date("Ym", strtotime("last month")), 'previous_plan');
		$this->framework->set('page', 'planing.html');
	}

	public function planProcessing() {
		$usage_handler = new Handler($this->database, 'purchasing');
		$count = $usage_handler->countRecord('id', date("Ym"));

		if ($this->framework->get('VERB') == 'POST') {
			$this->framework->set('DATA', array(
				'id'                  => date("Ym"),
				'purchasing_plan'     => $this->framework->get('POST.purchase_plan'),
				'purchasing_approved' => $this->framework->get('POST.purchase_approved'))
			);

			if ($count > 0) {
				$usage_handler->updateRecord('id', date("Ym"));
			}
			else {
				$usage_handler->createRecord();
			}

			$this->framework->reroute($this->framework->get('POST.referrer'), false);
		}
		else {
			$this->framework->reroute('/', false);
		}
	}
}