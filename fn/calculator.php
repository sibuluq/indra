<?php
class Calculator {
	protected
		$database,
		$framework;

	public function __construct() {
		$this->framework = Base::instance();
		$this->database  = new DB\SQL($this->framework->get('DATABASE'));
	}

	private function days() {
		switch (date("m")) {
			case '08':
				$days = 31;
				break;
			default:
				$days = 30;
				break;
		}

		return $days;
	}

	private function getData($table, $count_col, $count_filter) {
		$handler = new Handler($this->database, $table);
		$data['usage']   = $handler->loadAll();
		$data['count']   = $handler->countRecord($count_col, $count_filter);

		return $data;
	}

	public function calculateBalance($table, $count_col, $count_filter) {
		return $this->framework->get('TANKCAP') - $this->calculateUsageMonthly($table, $count_col, $count_filter);
	}

	public function calculateBalancePreviousMonth() {
		$handler = new Handler($this->database, 'monthlyusage');

		$handler->readRecord('month', date("Ym", strtotime("last month")), 'previous_month');
		if ($this->framework->get('previous_month.usage') == null) {
			return $this->framework->get('STOKAWAL');
		}
		else {
			return $this->framework->get('TANKCAP') - $this->framework->get('previous_month.usage');
		}
	}

	public function calculateBuffer($table, $count_col, $count_filter) {
		//return $this->framework->get('STOKAWAL') - $this->calculateUsageForecast($table, $count_col, $count_filter);
		return $this->calculateUsagePreviousMonth() - $this->calculateUsageForecast($table, $count_col, $count_filter);

	}

	public function calculateBufferDays($table, $count_col, $count_filter) {
		return $this->calculateBuffer($table, $count_col, $count_filter) / $this->calculateUsageAverage($table, $count_col, $count_filter);
	}

	public function calculateBufferTotal($table, $count_col, $count_filter) {
		return ($this->calculateBuffer($table, $count_col, $count_filter) + $this->calculateUsageForecast($table, $count_col, $count_filter)) / $this->calculateUsageAverage($table, $count_col, $count_filter);
	}

	public function calculateNextStock($table, $count_col, $count_filter) {
		return $this->calculatePurchasePlan($table, $count_col, $count_filter) + $this->calculateBuffer($table, $count_col, $count_filter);
	}

	public function calculateNextStockApproved($table, $count_col, $count_filter) {
		$handler = new Handler($this->database, 'purchasing');

		$handler->readRecord('id', date("Ym"), 'approval');

		return ($this->framework->get('approval.purchasing_approved') + $this->calculateBuffer($table, $count_col, $count_filter));
	}

	public function calculatePurchasePlan($table, $count_col, $count_filter) {
		return ($this->calculateBufferTotal($table, $count_col, $count_filter) - $this->calculateBufferDays($table, $count_col, $count_filter)) * $this->calculateUsageAverage($table, $count_col, $count_filter);
	}

	public function calculateUsageAverage($table, $count_col, $count_filter) {
		$data   = $this->getData($table, $count_col, $count_filter);
		return ($this->calculateUsageMonthly($table, $count_col, $count_filter) / $data['count']);
	}

	public function calculateUsageForecast($table, $count_col, $count_filter) {
		return $this->calculateUsageAverage($table, $count_col, $count_filter) * $this->days();
	}

	public function calculateUsageMonthly($table, $count_col, $count_filter) {
		$data   = $this->getData($table, $count_col, $count_filter);
		$result = 0;

		foreach(($data['usage']?:array()) as $value) {
			if ($value['month'] == date("Ym")) {
				$result = $result + $value['usage'];
			}
		}

		return $result;
	}

	public function calculateUsagePreviousMonth() {
		$handler = new Handler($this->database, 'monthlyusage');

		$handler->readRecord('month', date("Ym", strtotime("last month")), 'previous_month');
		if ($this->framework->get('previous_month.usage') == null) {
			return $this->framework->get('STOKAWAL');
		}
		else {
			return $this->framework->get('TANKCAP') - $this->framework->get('previous_month.usage');
		}
	}
}