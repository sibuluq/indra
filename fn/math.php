<?php
class Math extends Controller {
	private $table;

	private function getDatabase() {
		$handler = new Handler($this->database, $this->table);
		$data['usage']   = $handler->loadAll();
		$data['count']   = $handler->countRecord('month', date("Ym"));

		return $data;
	}

	public function calculateAverageUsage($table) {
		$this->table = $table;
		$data  = $this->getDatabase();
		return ($this->calculateTotalUsage($this->table) / $data['count']);
	}

	public function calculateBalance() {
		return $this->framework->get('TANKCAP') - $this->calculateUsageMonthly($this->table);
	}

	public function calculateBufferDays() {
		return $this->calculateBufferStock() / $this->calculateAverageUsage($this->table);
	}

	public function calculateBufferStock() {
		return $this->framework->get('STOKAWAL') - $this->calculateUsageForecast();
	}

	public function calculateBufferTotal() {
		return ($this->calculateBufferStock() + $this->calculateUsageForecast()) / $this->calculateAverageUsage($this->table);
	}

	public function calculatePurchasePlan() {
		return ($this->calculateBufferTotal() - $this->calculateBufferDays()) * $this->calculateAverageUsage($this->table);

	}

	public function calculateNextStock() {
		return $this->calculatePurchasePlan() + $this->calculateBufferStock();
	}

	public function calculateNextStockApproved() {
		$handler = new Handler($this->database, 'purchasing');

		$handler->readRecord('id', date("Ym"), 'approval');

		return ($this->framework->get('approval.purchasing_approval') + $this->calculateBufferStock());
	}

	public function calculateTotalUsage($table) {
		$this->table = $table;
		$data   = $this->getDatabase();
		$result = 0;

		foreach(($data['usage']?:array()) as $value) {
			$result = $result + $value['usage'];
		}

		return $result;
	}

	public function calculateUsageForecast() {
		return $this->calculateAverageUsage($this->table) * $this->days();
	}

	public function calculateUsageMonthly($table) {
		$this->table = $table;
		$data   = $this->getDatabase();
		$result = 0;

		foreach(($data['usage']?:array()) as $value) {
			if ($value['month'] == date("Ym")) {
				$result = $result + $value['usage'];
			}
		}

		return $result;
	}

	public function calculateUsagePreviousMonth() {
		$handler = new Handler($this->database, 'monthlyusage');

		$handler->readRecord('month', date("Ym", strtotime("last month")), 'previous_month');
		if ($this->framework->get('previous_month.usage') == null) {
			return $this->framework->get('STOKAWAL');
		}
		else {
			return $this->framework->get('TANKCAP') - $this->framework->get('previous_month.usage');
		}
	}

	public function calculateUsageYearly($table) {
		$this->table = $table;
		$data   = $this->getDatabase();
		$result = 0;

		foreach(($data['usage']?:array()) as $value) {
			if ($value['year'] == date("Y")) {
				$result = $result + $value['usage'];
			}
		}

		return $result;
	}
}

/* Akhir dari berkas math.php */