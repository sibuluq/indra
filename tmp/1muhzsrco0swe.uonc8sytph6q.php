<div class="uk-grid">
	<div class="uk-width-1-2">
		<table class="uk-table uk-table-hover">
			<tr>
				<th>Kapasitas tanki</th>
				<td><?php echo number_format($TANKCAP); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th title="Stok awal diambil dari sisa bulan kemarin." data-uk-tooltip>Stok awal</th>
				<td><?php echo number_format($previous); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Sisa tanki</th>
				<td><?php echo number_format($sisa); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Pemakaian rata-rata harian</th>
				<td><?php echo number_format($average); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Prakiraan pemakaian bulan ini</th>
				<td><?php echo number_format($forecast); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Pemakaian bulan ini</th>
				<td><?php echo number_format($total); ?></td>
				<td>liter</td>
			</tr>
		</table>
	</div>
	<div class="uk-width-1-2">
		<table class="uk-table uk-table-hover">
			<tr>
				<th>Buffer stock</th>
				<td><?php echo number_format($buffer); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Prakiraan buffer stock</th>
				<td><?php echo number_format($bufferday); ?></td>
				<td>hari</td>
			</tr>
			<tr>
				<th>Prakiraan total buffer stock</th>
				<td><?php echo number_format($buffertotal); ?></td>
				<td>hari</td>
			</tr>
			<tr>
				<th>Purchase plan</th>
				<td><?php echo number_format($purchase_plan); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Next stock</th>
				<td><?php echo number_format($next_stock); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Management approval</th>
				<td><?php echo number_format($approval['purchasing_approved']); ?></td>
				<td>liter</td>
			</tr>
			<tr>
				<th>Aktualisasi next stock</th>
				<td><?php echo number_format($actual); ?></td>
				<td>liter</td>
			</tr>
		</table>
	</div>
</div>