<div class="uk-container uk-container-center uk-margin-top">
	<?php echo $this->render('navbar.html',$this->mime,get_defined_vars()); ?>
	<div class="uk-grid uk-margin-large-bottom">
		<div class="uk-width-1-2">
			<form class="uk-form uk-form-horizontal" method="post" action="processing/plan">
				<div class="uk-form-row">
					<label class="uk-form-label" for="purchase_plan">Purchase plan</label>
					<div class="uk-form-controls">
						<input type="text" name="purchase_plan"> <span class="uk-form-help-inline">liter</span>
					</div>
				</div>
				<div class="uk-form-row">
					<label class="uk-form-label" for="purchase_approved">Management approval</label>
					<div class="uk-form-controls">
						<input type="text" name="purchase_approved"> <span class="uk-form-help-inline">liter</span>
					</div>
				</div>
				<div class="uk-form-row">
					<div class="uk-form-controls">
						<input type="submit" class="uk-button" value="Ubah">
						<input name="referrer" type="hidden" value="<?php echo $REALM; ?>">
					</div>
				</div>
			</form>
		</div>
		<div class="uk-width-1-2">
			<table class="uk-table uk-table-hover">
				<tr>
					<th>Purchase plan bulan lalu</th>
					<td><?php echo number_format($previous_plan['purchase_plan']); ?></td>
					<td>liter</td>
				</tr>
				<tr>
					<th>Purchase approved bulan lalu</th>
					<td><?php echo number_format($previous_approved); ?></td>
					<td>liter</td>
				</tr>
			</table>
		</div>
	</div>
	<?php echo $this->render('table.html',$this->mime,get_defined_vars()); ?>
</div>