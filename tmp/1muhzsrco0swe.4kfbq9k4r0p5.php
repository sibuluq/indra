<div class="uk-container uk-container-center uk-margin-top">
	<?php echo $this->render('navbar.html',$this->mime,get_defined_vars()); ?>
	<h2><?php echo $document['title'] . ': ' . date("l, d F Y"); ?></h2>
	<div class="uk-grid uk-margin-large-bottom">
		<div class="uk-width-3-5">
			<?php if ($usage != null): ?>
				
					<canvas id="daily-usage"></canvas>
				
				<?php else: ?><p>Belum ada data pemakaian.</p>
			<?php endif; ?>
		</div>
		<div class="uk-width-2-5">
			<form class="uk-form uk-form-stacked" method="post" action="processing/daily">
				<label for="dailyusage">Pemakaian hari ini (<?php echo date("Y-m-d"); ?>)</label>
				<div class="uk-form-controls">
					<input name="dailyusage" type="text">
					<input name="referrer" type="hidden" value="<?php echo $REALM; ?>">
					<input class="uk-button" name="update-daily-usage" type="submit" value="Update Pemakaian">
				</div>
			</form>
			<form class="uk-form uk-form-stacked uk-margin uk-margin-large-bottom" method="post" action="processing/monthly">
				<label for="monthlyusage">Pemakaian bulan ini (<?php echo date("Y-m"); ?>)</label>
				<div class="uk-form-controls">
					<input class="uk-form-blank" name="monthlyusage" type="text" value="<?php echo $total; ?>" readonly>
					<input class="uk-button" type="submit" value="Update Pemakaian">
					<input name="referrer" type="hidden" value="<?php echo $REALM; ?>">
				</div>
			</form>
		</div>
	</div>
	<?php echo $this->render('table.html',$this->mime,get_defined_vars()); ?>
</div>
<script>
	var config = {
		type: 'line',
		data: {
			labels: [<?php foreach (($usage?:array()) as $data): ?><?php if ($data['month'] == date('Ym')): ?>"<?php echo $data['date']; ?>",<?php endif; ?><?php endforeach; ?>],
			datasets: [{
				label: "Pemakaian",
				data: [<?php foreach (($usage?:array()) as $data): ?><?php if ($data['month'] == date('Ym')): ?>"<?php echo $data['usage']; ?>",<?php endif; ?><?php endforeach; ?>]
			}]
		},
		options: {
			responsive: true
		}
	};

	window.onload = function() {
		var context = document.getElementById("daily-usage").getContext("2d");
		window.usageChart = new Chart(context, config);
	};
</script>