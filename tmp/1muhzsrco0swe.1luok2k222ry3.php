<div class="uk-container uk-container-center uk-margin-top">
	<ul class="uk-grid uk-grid-width-1-3" data-uk-grid-margin>
		<?php foreach (($menu?:array()) as $data): ?>
			<a href="<?php echo $data['url']; ?>">
				<li>
					<div class="uk-panel uk-panel-box uk-text-center">
						<h3 class="uk-panel-title"><?php echo $data['label']; ?></h3>
						<i class="<?php echo $data['icon']; ?> uk-icon-large"></i>
					</div>
				</li>
			</a>
		<?php endforeach; ?>
	</ul>
</div>