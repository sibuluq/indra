<nav class="uk-navbar uk-margin-large-bottom">
	<ul class="uk-navbar-nav">
		<li><a href="/">Main Menu</a></li>
		<?php foreach (($menu?:array()) as $data): ?>
			<li><a href="<?php echo $data['url']; ?>"><?php echo $data['label']; ?></a></li>
		<?php endforeach; ?>
	</ul>
</nav>