<div class="uk-container uk-container-center uk-margin-top">
	<?php echo $this->render('navbar.html',$this->mime,get_defined_vars()); ?>
	<h2><?php echo $document['title']; ?></h2>
	<div class="uk-grid uk-margin-large-bottom">
		<div class="uk-width-3-5">
			<?php if ($usage != null): ?>
				
					<canvas id="monthly-usage"></canvas>
				
				<?php else: ?><p>Belum ada data pemakaian.</p>
			<?php endif; ?>
		</div>
		<div class="uk-width-2-5">
			<form class="uk-form uk-form-stacked uk-margin uk-margin-large-bottom" method="post" action="processing/monthly">
				<label for="monthlyusage">Pemakaian bulan ini (<?php echo date("Y-m"); ?>)</label>
				<div class="uk-form-controls">
					<input class="uk-form-blank" name="monthlyusage" type="text" value="<?php echo $total; ?>" readonly>
					<input class="uk-button" type="submit" value="Update Pemakaian">
					<input name="referrer" type="hidden" value="<?php echo $REALM; ?>">
				</div>
			</form>
		</div>
	</div>
	<?php echo $this->render('table.html',$this->mime,get_defined_vars()); ?>
</div>
<script>
	var config = {
		type: 'bar',
		data: {
			labels: [<?php foreach (($usage?:array()) as $data): ?><?php if ($data['year'] == date('Y')): ?>"<?php echo $data['label']; ?>",<?php endif; ?><?php endforeach; ?>],
			datasets: [{
				data: [<?php foreach (($usage?:array()) as $data): ?><?php if ($data['year'] == date('Y')): ?>"<?php echo $data['usage']; ?>",<?php endif; ?><?php endforeach; ?>]
			}]
		},
		options: {}
	};

	window.onload = function() {
		var context = document.getElementById("monthly-usage").getContext("2d");
		var usageChart = new Chart(context, config);
	};
</script>