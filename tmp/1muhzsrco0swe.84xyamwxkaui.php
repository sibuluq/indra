<!doctype html>
<html lang="id">
	<head>
		<meta charset="<?php echo $ENCODING; ?>">
		<base href="<?php echo $SCHEME . '://' . $site['url']; ?>">
		<title><?php echo $site['name']; ?></title>
		<?php foreach (($site['meta']?:array()) as $name=>$content): ?>
			<meta name="<?php echo $name; ?>" content="<?php echo $content; ?>">
		<?php endforeach; ?>
		<?php foreach (($site['asset']['css']?:array()) as $file): ?>
			<link rel="stylesheet" href="<?php echo 'ui/css/' . $file; ?>">
		<?php endforeach; ?>
		<?php foreach (($site['asset']['js']?:array()) as $file): ?>
			<script src="<?php echo 'ui/js/' . $file; ?>"></script>
		<?php endforeach; ?>
	</head>
	<body>
		<?php echo $this->render($page,$this->mime,get_defined_vars()); ?>
	</body>
</html>